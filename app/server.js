let express = require("express"),
    app=express();
let fs = require("fs");
let doc = fs.readFileSync('./doc/index.html');
const url = require('url');
let  scheduler = require ('./scheduler.js');
let timerCollection=[];
let apiService = require ('./apiService');
let apiResults = require ('./apiResults');
let Collection = require('./serviceCollection'),
        serviceCollection = new Collection();
let path    = require("path");
let request = require('request'),req;
var bodyParser = require('body-parser'),
     jsonParser = bodyParser.json();

app.use(express.static(__dirname + '/doc'));



app.listen(8080,()=>{
   apiService.getAll().then(services =>{
       services.forEach(function(element) {
         serviceCollection.Add({

            cron: scheduler.CronExe(element),
            id: element._id

        });  
       }, this);
       //scheduler.Start(services);  
   }) 
   
})

/**
 * @api {get} /results/:serviceid get results by service id
 * @apiGroup Results
 * 
 * @apiName GetResultsById
 * 
 * @apiParam {Number} serviceid id service for getting results

 * 
 * @apiSuccessExample Success-Response:
    {
        "_id": "58ce5b510bc5eb18a841cb1f",
        "date": "2017-03-19T10:20:01.785Z",
        "code": "200",
        "__v": 0,
        "service": [{
            "__v": 0,
            "time": "* /1 * * * *",
            "method": "get",
            "url": "http://osxh.ru/",
            "_id": "58ce5a1b0edb0c143c9441ca"
        }],
    }
 * 
 */
app.get("/results/:serviceid",(req,res)=>{
    // let arr= [];
    //  apiResults.getAll().then(document =>{
    //     document.forEach(function(element) {
    //         if(element.service.url==req.params.serv)
    //             arr.push(element);
    //     }, this);
    // res.send(arr);
    // });;
    console.log(req.params.serviceid);
    apiResults.getResByServ(req.params.serviceid).then( doc =>{
        res.send(doc);
    })
  
})

/**
 * @api {get} /results get all results
 * @apiGroup Results
 * 
 * @apiName GetAllResults
 * 
 * @apiSuccessExample Success-Response:
 * {
    "_id": "58cda0b99650d810a057fd5f",
    "date": "2017-03-18T21:03:53.994Z",
    "code": "200",
    "__v": 0,
    "service": [
    {
        "__v": 0,
        "time": "* * * * * *",
        "method": "get",
        "url": "http://osxh.ru/",
        "_id": "58cda01ec63bec208ca43b1e"
    }],
   },
  {
    "_id": "58cda0ba9650d810a057fd60",
    "date": "2017-03-18T21:03:54.042Z",
    "code": "200",
    "__v": 0,
    "service": [
    {
        "__v": 0,
        "time": "* * * * * *",
        "method": "get",
        "url": "http://osxh.ru/",
        "_id": "58cda01ec63bec208ca43b1e"
    }
    ],
  },
 * 
 * 
 */
app.get("/results",(req,res)=>{
apiResults.getAll().then(document =>{
        res.json(document);
    });;
    
})

/**
 * @api {post} /services Add new service to schedule
 * @apiName addService
 * 
 * @apiGroup Service
 * 
 * @apiParam {string} url Service url.
 * @apiParam {string} method post/get method.
 * @apiParam {string} time cron expression.
 * 
 * 
 * @apiSuccessExample Success-Response:
 *  {
        "__v": 0,
        "url": "http://osxh.ru/",
        "method": "get",
        "time": "* * * * * *",
        "_id": "58ce57c4764be12308ae9633"
    }
 */

app.post("/services", jsonParser, (req,res)=>{
    console.log(req.body.url);
    let time = req.body.time.replace(/[a-z]/gi," ");
    // let jsonQuery;
    // if(req.query.params){
    //     let params =JSON.parse(req.query.params);
    //     jsonQuery="?";
    //     for(prop in params ){

    //         jsonQuery+=`${prop}=${params[prop]}&`;
    //     }
    //     console.log(jsonQuery);
    // }
    let service = {
        url: req.body.url,
        method: req.body.method,
        //jsonParams:jsonQuery,
        time: time
    };
       
    apiService.addService(service).then(document =>{

        serviceCollection.Add({
            cron: scheduler.CronExe(document),
            id: document._id
        });

        res.json(document);
    });
   
   
})

/**
 * @api {delete} /services delete all services
 * @apiGroup Service
 * 
 * @apiName DeleteAllServices
 * 
 * @apiSuccess {String} Result All Service Delete
 * 
 * 
 */
app.delete("/services",(req,res)=>{
    apiService.deleteAll().
    then( ()=>{
        serviceCollection.DeleteAll();
        res.send("All Service Delete")
    })
})
/**
 * @api {delete} /results delete all results
 * @apiGroup Results
 * 
 * @apiName DeleteAllResults
 * 
 * @apiSuccess {String} Result  All Results Delete 
 * 
 * 
 */
app.delete("/results",(req,res)=>{
    apiResults.deleteAll().
    then( ()=>{
        res.send("All Results Delete")
    })
})
/**
 * @api {delete} /services/:id delete service by id
 * @apiGroup Service
 * 
 * @apiName DeleteService
 * @apiParam {Number} id Service unique ID.
 * 
 * @apiSuccess {String} Result  id deleted 
 * 
 */
app.delete("/services/:id",(req,res)=>{
    let id = req.params.id;
    
    apiService.delService(id).then(service =>{
            serviceCollection.Delete(id);
            res.send(`${service._id} deleted`);
        
       
    });

})
/**
 * @api {put} /services/:id update service by id
 * @apiGroup Service
 * 
 * @apiName ChangeService
 * 
 * @apiParam {Number} id id service to change
 * @apiParam {string} url Service url.
 * @apiParam {string} method post/get method.
 * @apiParam {string} time cron expression.
 * 
 * @apiSuccessExample Success-Response:
 *  {
        "__v": 0,
        "url": "http://osxh.ru/",
        "method": "get",
        "time": "* * * * * *",
        "_id": "58ce57c4764be12308ae9633"
    }
 * 
 */
app.put("/services/:id",jsonParser ,(req ,res)=>{
    let id = req.params.id;
    let time = req.body.time.replace(/[a-z]/gi," ");
    console.log(req.body.url);
    let newService = {
        url: req.body.url,
        method: req.body.method,
        time: time
    };
    
    apiService.changeService(id,newService).then(service =>{
        serviceCollection.Delete(id);
        serviceCollection.Add({
            cron: scheduler.CronExe(newService),
            id: service._id
        });    
        res.json(newService);
    })
    
})






/**
 * @api {get} /services get all services
 * @apiGroup Service
 * 
 * @apiName GetServices
 * @apiSuccessExample Success-Response:
 * {
    "_id": "58ce5a1b0edb0c143c9441ca",
    "url": "http://osxh.ru/",
    "method": "get",
    "time": " * /1 * * * *",
    "__v": 0
    },
  {
    "_id": "58ce5a1d0edb0c143c9441cb",
    "url": "http://osxh.ru/",
    "method": "get",
    "time": "* /1 * * * *",
    "__v": 0
    }
 * 
 * 
 */
app.get("/services",(req,res)=>{
apiService.getAll().then(document =>{
        res.json(document);
    });;
    
})
/**
* @api {get} /docs get  docs
* @apiGroup docs
* 
* @apiName Getdocs
*/
app.get("/docs",(req,res)=>{

    res.sendFile((path.join(__dirname+'/doc/index.html')));
})

