let mongoose    = require('mongoose');
mongoose.connect('mongodb://localhost/test1');
var db = mongoose.connection;

db.on('error', function (err) {
    
});
db.once('open', function callback () {
    console.log("Connected to DB!");
});
let ServiceModel = require('./serviceModel.js').ServiceModel;


exports.addService = (service)=> {
    let serv = new ServiceModel(service);
    return  serv.save();
}

exports.delService = (id)=> {
    return   ServiceModel.findById(id, (err, service)=>{
        if(!service) {
            return { error: 'Not found' };
        }
        return service.remove((err)=> {
            if (!err) {
                return { status: 'OK' };
            } else {
                res.statusCode = 500;
                return { error: 'Server error' };
            }
        });
    });
}

exports.changeService = (id, newService)=>{
    return   ServiceModel.findById(id, (err, service)=>{
        if(!service) {
            return { error: 'Not found' };
        }
        service.url = newService.url;
        service.time = newService.time;
        service.method = newService.method;
       return service.save();
    });
}
exports.getAll =  ()=> {
    return ServiceModel.find();
}

exports.deleteAll = ()=>{

    return ServiceModel.remove({});
}
