let CronJob = require('cron').CronJob;
let request = require('request'),req;
let apiResults = require ('./apiResults');
let apiService = require ('./apiService');

function CronExe(service){
     return new CronJob(service.time, function() {
    
        let req;
        if(service.method.toLowerCase() == "post")
            req = request.post;
        else req = request.get;

        req(service.url, (request, res)=>{
            console.log(service.url);
            let result = {
                        service: service,
                        date: new Date(),
                        code: res.statusCode
                }
            if(res.statusCode != 200)
            {
                extraReq(req, service);
            }
            apiResults.addResult(result);
        })
    }, null, true)
}

// function Start()
// {
//     apiService.getAll().then( (services)=>{
//             services.forEach(function(element) {
//                 CronExe(element);
//             }, this);
//     })
// }
function extraReq(request,service ,count=3){
        if(count!=0)
        request(service.url, (req, res)=>{
            let result = {
                service: service,
                date: new Date(),
                code: res.statusCode    
            }
           
            console.log(`extraREQ ${service.url} ${count}`);
            count--;
            apiResults.addResult(result);
            extraReq(request,service,count);
            
        })
}
exports.CronExe = CronExe;
//exports.Start = Start;