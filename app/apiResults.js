let mongoose    = require('mongoose');
mongoose.connect('mongodb://localhost/test2');
var db = mongoose.connection;

db.on('error', function (err) {
    
});
db.once('open', function callback () {
    console.log("Connected to DB!");
});



let ResultModel = require('./serviceModel.js').ResultModel;

exports.addResult = (result)=>{
    return new ResultModel(result).save();
}

exports.getAll = ()=>{
     return ResultModel.find();
}

exports.deleteAll = ()=>{
    
    return ResultModel.remove({});
}
exports.getResByServ = (serviceID)=>{
     return ResultModel.find({'service._id':serviceID});
}