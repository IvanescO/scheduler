let mongoose    = require('mongoose');

let Schema = mongoose.Schema;

// Schemas
let Service = new Schema({
    url: { type: String, required: true },
    method: String,
    //jsonParams: String,
    time: String
});

let ServiceResult = new Schema({
    service:[Service],
    date: Date,
    code: String
});

let ServiceModel = mongoose.model('Service', Service);
let ResultModel = mongoose.model('Result',ServiceResult)


module.exports.ServiceModel = ServiceModel;
module.exports.ResultModel = ResultModel;