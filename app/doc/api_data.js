define({ "api": [
  {
    "type": "delete",
    "url": "/results",
    "title": "delete all results",
    "group": "Results",
    "name": "DeleteAllResults",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Result",
            "description": "<p>All Results Delete</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Results"
  },
  {
    "type": "get",
    "url": "/results",
    "title": "get all results",
    "group": "Results",
    "name": "GetAllResults",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"_id\": \"58cda0b99650d810a057fd5f\",\n    \"date\": \"2017-03-18T21:03:53.994Z\",\n    \"code\": \"200\",\n    \"__v\": 0,\n    \"service\": [\n    {\n        \"__v\": 0,\n        \"time\": \"* * * * * *\",\n        \"method\": \"get\",\n        \"url\": \"http://osxh.ru/\",\n        \"_id\": \"58cda01ec63bec208ca43b1e\"\n    }],\n   },\n  {\n    \"_id\": \"58cda0ba9650d810a057fd60\",\n    \"date\": \"2017-03-18T21:03:54.042Z\",\n    \"code\": \"200\",\n    \"__v\": 0,\n    \"service\": [\n    {\n        \"__v\": 0,\n        \"time\": \"* * * * * *\",\n        \"method\": \"get\",\n        \"url\": \"http://osxh.ru/\",\n        \"_id\": \"58cda01ec63bec208ca43b1e\"\n    }\n    ],\n  },",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Results"
  },
  {
    "type": "get",
    "url": "/results/:serviceid",
    "title": "get results by service id",
    "group": "Results",
    "name": "GetResultsById",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "serviceid",
            "description": "<p>id service for getting results</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"_id\": \"58ce5b510bc5eb18a841cb1f\",\n    \"date\": \"2017-03-19T10:20:01.785Z\",\n    \"code\": \"200\",\n    \"__v\": 0,\n    \"service\": [{\n        \"__v\": 0,\n        \"time\": \"* /1 * * * *\",\n        \"method\": \"get\",\n        \"url\": \"http://osxh.ru/\",\n        \"_id\": \"58ce5a1b0edb0c143c9441ca\"\n    }],\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Results"
  },
  {
    "type": "put",
    "url": "/services/:id",
    "title": "update service by id",
    "group": "Service",
    "name": "ChangeService",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id service to change</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>Service url.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "method",
            "description": "<p>post/get method.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "time",
            "description": "<p>cron expression.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n       \"__v\": 0,\n       \"url\": \"http://osxh.ru/\",\n       \"method\": \"get\",\n       \"time\": \"* * * * * *\",\n       \"_id\": \"58ce57c4764be12308ae9633\"\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Service"
  },
  {
    "type": "delete",
    "url": "/services",
    "title": "delete all services",
    "group": "Service",
    "name": "DeleteAllServices",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Result",
            "description": "<p>All Service Delete</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Service"
  },
  {
    "type": "delete",
    "url": "/services/:id",
    "title": "delete service by id",
    "group": "Service",
    "name": "DeleteService",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Service unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Result",
            "description": "<p>id deleted</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Service"
  },
  {
    "type": "get",
    "url": "/services",
    "title": "get all services",
    "group": "Service",
    "name": "GetServices",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"_id\": \"58ce5a1b0edb0c143c9441ca\",\n    \"url\": \"http://osxh.ru/\",\n    \"method\": \"get\",\n    \"time\": \" * /1 * * * *\",\n    \"__v\": 0\n    },\n  {\n    \"_id\": \"58ce5a1d0edb0c143c9441cb\",\n    \"url\": \"http://osxh.ru/\",\n    \"method\": \"get\",\n    \"time\": \"* /1 * * * *\",\n    \"__v\": 0\n    }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Service"
  },
  {
    "type": "post",
    "url": "/services",
    "title": "Add new service to schedule",
    "name": "addService",
    "group": "Service",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "url",
            "description": "<p>Service url.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "method",
            "description": "<p>post/get method.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "time",
            "description": "<p>cron expression.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n       \"__v\": 0,\n       \"url\": \"http://osxh.ru/\",\n       \"method\": \"get\",\n       \"time\": \"* * * * * *\",\n       \"_id\": \"58ce57c4764be12308ae9633\"\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Service"
  },
  {
    "type": "get",
    "url": "/docs",
    "title": "get  docs",
    "group": "docs",
    "name": "Getdocs",
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "docs"
  }
] });
