module.exports = class ServiceCollection{
    
    constructor(){
        console.log("collection created");
        this.collection = [];
    };

    Add(service){
        console.log(`add to collection ${service.id}`);
        this.collection.push(service);
    }

    Delete(id){

       for(let i =0 ; i<this.collection.length; i++)
        {   
            if(this.collection[i].id == id)
                {   this.collection[i].cron.stop();
                    this.collection.slice(i,1);
                    console.log("find id");
                    break;
                }
            
        }
        
    }
   
    Change(service){
        this.Delete(service.id);
        this.Add(service);
    }
    DeleteAll(){
       for(let i = 0 ; i<this.collection.length; i++)
        {
            this.collection[i].cron.stop();
        }
    }    
}
